
export default function Page() {
    return (
        <div>
            <div className="titleName">
                <h1>나만의 술집 찾기 </h1>
                <p className="text-2xl">술집 선택이 어려울 때, 나만의 숨겨진 명소를 함께 찾아봐요!</p>
            </div>
            <div className="imgg">
            <img src="/bear.jpeg"/>
            </div>
        </div>
    );
}
